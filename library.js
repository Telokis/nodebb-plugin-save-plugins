(function(module) {
    "use strict";

    var path = require("path");
    var nconf = module.parent.require("nconf");
    var storeInstalled = require("./lib/storeInstalled");

    var data = {
        filePath: path.join(nconf.get("base_dir"), "plugins.json"),
        pluginData: [],
        async: module.parent.require("async"),
        winston: module.parent.require("winston"),
        fs: require("fs"),
        Plugins: module.parent.require("./plugins"),
        _: module.parent.require("underscore")
    };

    var plugin = {};

    plugin.setup = function (args, callback) {
        if (!data.fs.existsSync(data.filePath)) {
            data.winston.info('No plugins.json file found. Defaulting to []');
            data.fs.writeFileSync(data.filePath, "[]");
        }
        data.async.series([
            (next) => storeInstalled.storeInstalled(data, next),
            (next) => require("./lib/installMissing")(data, next)
        ], callback);
    };

    plugin.uninstalledPlugin = function (id, callback) {
        data.pluginData = data.pluginData.filter(item => item !== id);
        storeInstalled.storeData(data.pluginData, data, callback);
    };

    plugin.installedPlugin = function (id, callback) {
        data.pluginData.push(id);
        storeInstalled.storeData(data.pluginData, data, callback);
    };

    module.exports = plugin;
}(module));