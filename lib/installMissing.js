(function (module) {
    module.exports = function (data, callback) {
        data.async.each(data.pluginData, function (plugin, done) {
            data.async.waterfall([
                (next) => data.Plugins.isInstalled(plugin, next),
                function (installed_, next) {
                    if (installed_)
                        return next(null);
                    data.winston.info("[nodebb-plugin-save-plugins] Installing %s.", plugin);
                    data.Plugins.toggleInstall(plugin, "latest", next)
                }
            ], done);
        }, callback);
    };
}(module));