(function (module) {
    var stringify = require('json-stable-stringify');

    module.exports.storeInstalled = function (data, callback) {
        data.async.waterfall([
            data.async.apply(data.fs.readFile, data.filePath, "utf8"),
            data.async.asyncify(JSON.parse),
            (arr, next) => {
                data.pluginData = arr;
                next();
            },
            data.Plugins.showInstalled,
            function (installedPlugins, next) {
                data.async.map(installedPlugins, (obj, nxt) => nxt(null, obj.id), next);
            },
            function (mapped, next) {
                next(null, mapped.concat(data.pluginData));
            },
            function (concated, next) {
                data.pluginData = concated;
                module.exports.storeData(concated, data, next);
            }
        ], callback);
    };

    module.exports.storeData = function (pluginData, data, callback) {
        data.async.waterfall([
            function (next) {
                next(null, pluginData);
            },
            data.async.asyncify(data._.uniq),
            function (uniq, next) {
                uniq.sort();
                next(null, stringify(uniq, {space: 4}));
            },
            function (formatted, next) {
                data.fs.writeFile(data.filePath, formatted, next);
            }
        ], callback);
    };

}(module));