_Current repository version : v1.1.0_

# nodebb-plugin-save-plugins
A plugin storing installed plugins in a plugins.json file. Automatically installs plugins if they are missing.

Simply activate it and it will store all installed plugins inside `<nodebb_root>/plugins.json`.
You can then share this file with people so that they can use your plugins.

On startup, every plugin contained inside `plugins.json` will be installed if they are missing.